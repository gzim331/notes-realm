//
//  PreviewViewController.swift
//  NotesRealm
//
//  Created by michal on 04/11/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit
import RealmSwift

class PreviewViewController: UIViewController {

    @IBOutlet weak var bodyTextView: UITextView!
    
    let realm = try! Realm()
    
    var note: Results<Note>?
    
    var selectedNote: Note? {
        didSet {
            loadNote()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bodyTextView.text = selectedNote?.body
        
    }
    
    @IBAction func doneButtonPressed(_ sender: UIBarButtonItem) {
        
        //if pusto -> usuń notatke
        
        if let additionalSelectedNote = selectedNote {
            let updateNote = additionalSelectedNote
            
            do {
                try realm.write {
                    updateNote.date = Date()
                    updateNote.body = bodyTextView.text
                }
            } catch {
                print("Error saving done status \(error)")
            }
        }
        
        navigationController?.popViewController(animated: true)
    }
    
//    func saveNote(note: Note) {
//        do {
//            try realm.write {
//
//            }
//        } catch {
//            print("Context couldn't be save")
//        }
//    }
    
    func loadNote() {
        let predicate = NSPredicate(format: "body BEGINSWITH [c]%@", selectedNote!.body)
        note = realm.objects(Note.self).filter(predicate)
    }
    
}
