//
//  NoteViewController.swift
//  NotesRealm
//
//  Created by michal on 04/11/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit
import RealmSwift

class NoteViewController: UITableViewController {

    let realm = try! Realm()
    
    var notes: Results<Note>?
    let calendar = Calendar.current
    
    var selectedFolder: Folder? {
        didSet {
            loadNotes()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath)
        
        let date = notes?[indexPath.row].date
        
        cell.textLabel?.text = "New Note \(indexPath.row)"
        
        if let additionalDate = date {
            cell.detailTextLabel?.text = showDate(date: additionalDate)
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! PreviewViewController
        
        if let indexPath = tableView.indexPathForSelectedRow {
            destinationVC.selectedNote = notes?[indexPath.row]
        }
    }
    
    // MARK: - Table view delegate methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "goToPreview", sender: self)
    }
    
    // MARK: - Add new note
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        if let currentFolder = self.selectedFolder {
            do {
                try self.realm.write {
                    let newNote = Note()
                    newNote.date = Date()
                    newNote.body = ""
                    currentFolder.notes.append(newNote)
                }
            } catch {
                print("Error save new item, \(error)")
            }
        }
        
        self.tableView.reloadData()
        //        performSegue(withIdentifier: "goToPreview", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
//            context.delete(notes[indexPath.row])
//            notes.remove(at: indexPath.row)
//            saveNotes()
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.left)
        }
    }
    
    
    // MARK: - Data manipulation methods
    
//    func saveNotes() {
//        do {
//            try context.save()
//        } catch {
//            print("Context couldn't be save")
//        }
//    }
    
    func loadNotes() {
        
        notes = selectedFolder?.notes.sorted(byKeyPath: "body", ascending: true)
        
        tableView.reloadData()
    }

}
