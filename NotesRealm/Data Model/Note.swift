//
//  Note.swift
//  NotesRealm
//
//  Created by michal on 04/11/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import Foundation
import RealmSwift

class Note: Object {
    @objc dynamic var body: String = ""
    @objc dynamic var date: Date?
    var parentFolder = LinkingObjects(fromType: Folder.self, property: "notes")
}
