//
//  Folder.swift
//  NotesRealm
//
//  Created by michal on 04/11/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import Foundation
import RealmSwift

class Folder: Object {
    @objc dynamic var name: String = ""
    let notes = List<Note>()
}
